import java.util.Objects;

public class BinarySearchTree<E extends Comparable<E>> {
    protected Node<E> root;

    BinarySearchTree() {
        root = null;
    }

    public void insert(E key) {
        root = insertRec(root, key);
    }

    private Node<E> insertRec(Node<E> root, E data) {
        if (root == null) {
            root = new Node<>(data);
            if (this.root == null) {
                this.root = root;
            }
            return root;
        } else {
            Node tempNode = null;
            if (data.compareTo(root.item) <= 0) {
                tempNode = insertRec(root.left, data);
                root.left = tempNode;
                tempNode.parent = root;

            } else {
                tempNode = insertRec(root.right, data);
                root.right = tempNode;
                tempNode.parent = root;
            }

            return root;
        }
    }

    protected Node<E> inOrderSuccessor(Node<E> node) {

        if (node.right != null) {
            return minimum(node.right);
        }

        Node<E> parent = node.parent;
        while (parent != null && node == parent.right) {
            node = parent;
            parent = parent.parent;
        }
        return parent;
    }


    E delete(E key) {
        return delete(binarySearch(this.root, key));
    }

    protected E delete(Node<E> node) {
        Node<E> nodeToDelete;
        Node<E> child;
        if (node.left == null || node.right == null) {
            nodeToDelete = node;
        } else {
            nodeToDelete = inOrderSuccessor(node);
        }

        if (node.left != null) {
            child = nodeToDelete.left;
        } else {
            child = nodeToDelete.right;
        }

        if(child != null) {
            child.parent = nodeToDelete.parent;
        }
        if (nodeToDelete.parent == null) {
            this.root = child;
        } else if(nodeToDelete.equals(nodeToDelete.parent.left)) {
            nodeToDelete.parent.left = child;
        } else {
            nodeToDelete.parent.right = child;
        }

        if(!node.equals(nodeToDelete)) {
            node.item = nodeToDelete.item;
        }

        return nodeToDelete.item;
    }

    public Node<E> minimum(Node<E> root) {
        if (root.left != null) {
            return minimum(root.left);
        } else {
            return root;
        }
    }

    public Node<E> maximum(Node<E> root) {
        if (root.right != null) {
            return maximum(root.right);
        } else {
            return root;
        }
    }

    public Node<E> binarySearch(Node<E> root, E key) {
        if (root == null || root.item == key) {
            return root;
        } else if (root.item.compareTo(key) < 0) {
            return binarySearch(root.right, key);
        } else {
            return binarySearch(root.left, key);
        }
    }

    void inOrder(Node root) {
        if (root != null) {
            inOrder(root.left);
            System.out.println(root.item);
            inOrder(root.right);
        }
    }

    public boolean isEmpty() {
        return this.root == null;
    }


    protected static class Node<E extends Comparable<E>> {
        E item;
        Node<E> left;
        Node<E> right;
        Node<E> parent;

        Node(Node<E> left, E element, Node<E> right, Node<E> parent) {
            this.item = element;
            this.left = left;
            this.right = right;
            this.parent = parent;
        }

        Node(E element) {
            this.item = element;
            this.left = null;
            this.right = null;
            this.parent = null;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Node<?> node = (Node<?>) o;
            return item == node.item &&
                    Objects.equals(left, node.left) &&
                    Objects.equals(right, node.right) &&
                    Objects.equals(parent, node.parent);
        }

        @Override
        public int hashCode() {
            return Objects.hash(item, left, right, parent);
        }
    }
}
