import java.util.concurrent.ThreadLocalRandom;

import static java.lang.System.exit;

public class Main {
    public static void main(String[] args) {
        final int min = 1;
        final int max = 1000;
        PriorityQueueBinaryTree tree = new PriorityQueueBinaryTree();
        PriorityQueueLinkedList linkedList = new PriorityQueueLinkedList();
        System.out.println("******************************STARTING******************************");
        for (int i = 0; i < 50; i++) {
            int randomNum = ThreadLocalRandom.current().nextInt(min, max + 1);
            System.out.println( (i +1) +" random got: " + randomNum);
            tree.enqueue(randomNum);
            linkedList.enqueue(randomNum);
        }
        int queries = 0;
        System.out.println("STARTING DEQUEUE!!");
        int treeVal;
        int listVal;
        while ((!tree.isEmpty() && !linkedList.isEmpty())) {
            treeVal = tree.dequeue();
            listVal = linkedList.dequeue();
            System.out.println("query number " + ++queries + ": tree query: " + treeVal + "           list query: " + listVal);
            if (treeVal != listVal) {
                System.out.println("ERROR: the numbers above are not equal!!  tree: " + treeVal + "   list: " + listVal);
                exit(-1);
            }
        }
    }
}
