public interface Queue {
    boolean isEmpty();

    int dequeue();

    void enqueue(int value);
}
