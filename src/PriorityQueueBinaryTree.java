public class PriorityQueueBinaryTree extends BinarySearchTree<Integer> implements Queue {

    public PriorityQueueBinaryTree() {
    }

    @Override
    public boolean isEmpty() {
        return super.isEmpty();
    }

    @Override
    public int dequeue() {
        return super.delete(super.maximum(this.root));
    }

    @Override
    public void enqueue(int value) {
        super.insert(value);
    }
}
