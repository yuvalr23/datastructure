public class LinkedList <E extends Comparable<E>>{

    private int size;
    private Node<E> first;
    private Node<E> last;

    LinkedList() {
        this.size = 0;
        this.first = null;
        this.last = null;
    }

    public Node<E> getHead() {
        return this.first;
    }

    public boolean isEmpty() {
        return this.size == 0;
    }

    private void insertFirst(E value) {
        Node<E> currFirst = this.first;
        Node<E> newNode = new Node<>(null, value, currFirst);
        this.first = newNode;
        if (currFirst == null) {
            newNode.prev = newNode;
            newNode.next = newNode;
            this.last = newNode;
        } else {
            newNode.prev = currFirst.prev;
            currFirst.prev = newNode;
        }

        ++this.size;
    }

    public void insert (E value) {
        boolean flag = false;
        Node<E> node = this.getHead();
        do {
            if (node == null) {
                insertFirst(value);
                flag = true;
            } else if(value.compareTo(node.item) > 0) {
                insertBefore(value, node);
                flag = true;
            }else {
                node = node.next;
            }
        } while (!flag && node != this.getHead());

        if (node == this.getHead() && !flag) {
            insertLast(value);
        }
    }

    private void insertLast(E value) {
        final Node<E> currLast = last;
        final Node<E> newNode = new Node<>(currLast, value, null);
        last = newNode;
        if (currLast == null) {
            newNode.prev = newNode;
            newNode.next = newNode;
            this.first = newNode;
        } else {
            newNode.next = currLast.next;
            currLast.next.prev = newNode;
            currLast.next = newNode;
        }
        size++;
    }

    protected E remove(Node<E> node) {
        E element = node.item;
        Node<E> next = node.next;
        Node<E> prev = node.prev;
        if (prev == last) {
            this.first = next;
            next.prev = last;
            prev.next = next;
        } else {
            prev.next = next;
            next.prev = prev;
            node.prev = null;
        }

        if (next == first) {
            this.last = prev;
        } else {
            node.next = null;
        }

        node.item = null;
        --this.size;
        return element;
    }

    public E remove() {
        return remove(this.first);
    }

    private void insertBefore(E newValue, Node<E> nextNode) {
        final Node<E> prevNode = nextNode.prev;
        final Node<E> newNode = new Node<>(prevNode, newValue, nextNode);
        nextNode.prev = newNode;
        if (prevNode == nextNode || nextNode == this.first) {
            this.first = newNode;
        }
        prevNode.next = newNode;
        size++;
    }

    protected static class Node<E extends Comparable<E>> {
        E item;
        Node<E> next;
        Node<E> prev;

        Node(Node<E> prev, E element, Node<E> next) {
            this.item = element;
            this.next = next;
            this.prev = prev;
        }
    }
}


