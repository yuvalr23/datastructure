public class PriorityQueueLinkedList extends  LinkedList<Integer> implements Queue{

    public PriorityQueueLinkedList() {
        super();
    }

    @Override
    public boolean isEmpty() {
        return super.isEmpty();
    }

    @Override
    public int dequeue() {
        return super.remove();
    }

    @Override
    public void enqueue(int value) {
        insert(value);
    }
}
